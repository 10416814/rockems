﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace WindowsGame1
{
    struct Rock
    {
        public Vector2 position;
        public Vector2 direction;
        public float speed;
        public bool isActive;

        public void Update(float delta, float random)
        {
            position += direction * speed * delta;

            if (position.X > GameConstants.GraphicsWidth)
                position.X -= GameConstants.GraphicsWidth;
            if (position.X < -GameConstants.GraphicsWidth)
                position.X += GameConstants.GraphicsWidth;
            if (position.Y > GameConstants.GraphicsHeight)
            {
                position.Y = -100;
                position.X = random;
            }
            if (position.Y < -GameConstants.GraphicsHeight)
                position.Y += GameConstants.GraphicsHeight;
        }
    }
}
