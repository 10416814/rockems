using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private KeyboardState oldState;
        private SpriteBatch spriteBatch;
        private Color backColor = Color.CornflowerBlue;
        private Vector2 playerPosition;
        private Vector2 grassPosition;
        private Vector2 spriteSpeed;
        // This is a texture we can render.
        private Texture2D rock;
        private Texture2D player2D;
        private Texture2D playerAlive;
        private Texture2D playerDead;
        private Texture2D gem2D;
        private Texture2D grass;
        private Gem gem;
        private Player player;
        private int highestScore;

        Random _r;
        Rock[] rockList = new Rock[GameConstants.NumRocks];
        static int index = 0;

        private static System.Timers.Timer t;

        SpriteFont kootenay;
        int score;
        Vector2 scorePosition = new Vector2(600, 440);
        Vector2 highestScorePosition = new Vector2(400, 440);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            _r = new Random();
            gem = new Gem();
            score = 0;
            highestScore = 0;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            oldState = Keyboard.GetState();
            playerPosition = new Vector2(graphics.GraphicsDevice.Viewport.TitleSafeArea.Center.X, 300.0f);
            grassPosition = new Vector2(graphics.GraphicsDevice.Viewport.TitleSafeArea.Left, 390.0f);
            // Store some information about the sprite's motion.
            spriteSpeed = new Vector2(0.0f, 50.0f);
            // Set the coordinates to draw the sprite at.
            //gemPosition = new Vector2(0.0f, -100);
            ResetRocks();
            player.isActive = true;
            ResetGem();

            System.Timers.Timer t = new System.Timers.Timer(1000); // This creates a new timer that will fire every second (1000 milliseconds)
            t.Elapsed += new System.Timers.ElapsedEventHandler(UpdateEvent); // Register the function with the timer
            t.Enabled = true; // Start the timer!

            base.Initialize();
        }



        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            rock = Content.Load<Texture2D>("Rock");
            player2D = Content.Load<Texture2D>("Character Boy");
            playerAlive = Content.Load<Texture2D>("Character Boy");
            playerDead = Content.Load<Texture2D>("Character Boy - Dead");
            gem2D = Content.Load<Texture2D>("Gem Orange sml");
            grass = Content.Load<Texture2D>("Grass Block");
            kootenay = Content.Load<SpriteFont>("Kootenay");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            UpdateInput(gameTime);

            float timeDelta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            for (int i = 0; i < GameConstants.NumRocks; i++)
            {
                if (rockList[i].isActive)
                {
                    rockList[i].Update(timeDelta, _r.Next(graphics.GraphicsDevice.Viewport.Width - rock.Width));
                }
            }

            gem.Update();
            UpdateCollision();

            base.Update(gameTime);
        }


        private void UpdateEvent(object o, ElapsedEventArgs e)
        {
            score += 1;


            if (e.SignalTime.Second % 10 == 0)
            {
                if (index >= GameConstants.NumRocks)
                {
                    index = 0;
                    ResetRocks();
                }
                rockList[index].isActive = true;
                index++;

                Console.WriteLine("UpdateEvent: Gem ");

                gem.position.X = _r.Next(graphics.GraphicsDevice.Viewport.Width - gem2D.Width);

                if (gem.isActive)
                    gem.isActive = false;
                else
                    gem.isActive = true;
            }
        }

        public static bool BoundingCircle(int x1, int y1, int radius1, int x2, int y2, int radius2)
        {
            Vector2 V1 = new Vector2(x1, y1); // reference point 1
            Vector2 V2 = new Vector2(x2, y2); // reference point 2
            Vector2 Distance = V1 - V2; // get the distance between the two reference points
            if (Distance.Length() < radius1 + radius2) // if the distance is less than the diameter
                return true;

            return false;
        }

        private void UpdateCollision()
        {
            //player-rock collision check
            for (int i = 0; i < rockList.Length; i++)
            {
                if (BoundingCircle((int)playerPosition.X, (int)playerPosition.Y, (player2D.Width / 2), (int)rockList[i].position.X, (int)rockList[i].position.Y, ((rock.Width - 45) / 2)) == true)
                {
                    player2D = playerDead;
                    player.isActive = false;
                    if (highestScore < score)
                        highestScore = score;
                    score = 0;
                }
            }

            //player-gem collision check
            if (BoundingCircle((int)(playerPosition.X + 40), (int)playerPosition.Y, ((player2D.Width) / 4), (int)gem.position.X, (int)(gem.position.Y-70), ((gem2D.Width + 20) / 2)) == true)
            {
                if (gem.isActive && player.isActive)
                {
                    score += 5;
                    Console.WriteLine("Score++");
                    gem.isActive = false;
                }
            }
        }

        private void UpdateInput(GameTime gameTime)
        {
            KeyboardState newState = Keyboard.GetState();
            int MaxX = graphics.GraphicsDevice.Viewport.Width - player2D.Width;
            int MinX = 0;

            if (newState.IsKeyDown(Keys.A) && playerPosition.X > MinX)
            {
                playerPosition.X -= 3;
            }
            if (newState.IsKeyDown(Keys.D) && playerPosition.X < MaxX)
            {
                playerPosition.X += 3;
            }

            if (newState.IsKeyDown(Keys.Space))
            {
                player2D = playerAlive;
                player.isActive = true;
            }

            // Update saved state.
            oldState = newState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(backColor);

            // Draw the sprite.
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            spriteBatch.Draw(player2D, playerPosition, Color.White);
            spriteBatch.End();

            for (int i = 0; i < GameConstants.NumRocks; i++)
            {
                if (rockList[i].isActive)
                {
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
                    spriteBatch.Draw(rock, rockList[i].position, Color.White);
                    spriteBatch.End();
                }
            }

            if (gem.isActive)
            {
                spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
                spriteBatch.Draw(gem2D, gem.position, Color.White);
                spriteBatch.End();
            }


            Rectangle floor = new Rectangle(0, 0, graphics.GraphicsDevice.Viewport.Width, grass.Height);
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            spriteBatch.Draw(grass, grassPosition, floor, Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            spriteBatch.End();
            
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            spriteBatch.DrawString(kootenay, "Score: " + score, scorePosition, Color.LightGreen);
            spriteBatch.DrawString(kootenay, "Highest score: " + highestScore, highestScorePosition, Color.LightGreen);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void ResetRocks()
        {
            for (int i = 0; i < GameConstants.NumRocks; i++)
            {
                rockList[i].position.X = _r.Next(graphics.GraphicsDevice.Viewport.Width);
                rockList[i].position.Y = -100;
                rockList[i].direction.Y = 100;
                rockList[i].speed = 1.0f;

                rockList[i].isActive = false;
            }
        }

        private void ResetGem()
        {
            gem.position.X = _r.Next(graphics.GraphicsDevice.Viewport.Width);
            gem.isActive = true;
        }
    }

    class GameConstants
    {
        public const int NumRocks = 10;

        public const float GraphicsWidth = 699;
        public const float GraphicsHeight = 309;

    }
}
